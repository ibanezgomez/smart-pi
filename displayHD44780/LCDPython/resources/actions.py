####################################################################################
# 
#   actions.py
#
#    This file is part of smartPi proyect
#
#    smartPi is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    smartPi is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with smartPi.  If not, see <http://www.gnu.org/licenses/>.
#
####################################################################################

#This file contains the accions called in .xbmc/userdata/keymaps/keymaps/keyboard.xml

import subprocess
import sys

def setPoweroff ():
  subprocess.call("sudo poweroff", shell=True)

def setReboot ():
  subprocess.call("sudo reboot", shell=True)

def setLCDPython ():
  subprocess.call("sudo python /home/pi/displayHD44780/LCDPython/main.py", shell=True)

takeaction = {
  "poweroff": setPoweroff,
  "reboot": setReboot,
  "lcdpython": setLCDPython
}
takeaction.get(str(sys.argv[1]))()
