#!/bin/
####################################################################################
# 
#   install.sh
#
#    This file is part of smartPi proyect
#
#    smartPi is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    smartPi is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with smartPi.  If not, see <http://www.gnu.org/licenses/>.
#
####################################################################################

sudo apt-get update
sudo apt-get install gcc rsync python-dev bc transmission-remote-cli nmap

#wget http://pypi.python.org/packages/source/R/RPi.GPIO/RPi.GPIO-0.5.2a.tar.gz
tar zxf RPi.GPIO-0.5.2a.tar.gz
cd RPi.GPIO-0.5.2a
sudo python setup.py install
cd ..
sudo rm -rf RPi.GPIO-0.5.2a/
#rm RPi.GPIO-0.5.2a.tar.gz

#Adding keyboard shortcurt keymap
rsync -aPz keyboard.xml /home/pi/.xbmc/userdata/keymaps/

#Wallpaper swap
sudo cp SKINDEFAULT.jpg /opt/xbmc-bcm/xbmc-bin/share/xbmc/addons/skin.confluence/backgrounds/

#sudo echo "#!/bin/sh -e
#python /home/pi/LCDPython/main.py
#exit 0" > /etc/rc.local


