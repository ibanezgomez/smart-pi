####################################################################################
# 
#   Network.py
#
#    This file is part of smartPi proyect
#
#    smartPi is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    smartPi is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with smartPi.  If not, see <http://www.gnu.org/licenses/>.
#
####################################################################################

import socket
import subprocess
import fcntl
import struct
import logging

logging.basicConfig(format='%(asctime)s - [%(name)s]     - %(levelname)s - %(filename)s - %(lineno)d - %(message)s' ,level=logging.INFO)

def getIP(dev):
  logging.info("[Started] getIP call")     
  try:  
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(s.fileno(),0x8915,struct.pack('256s', dev))[20:24])
  except:
    logging.warning("[Finish] getIP call, UNKNOWN IP")
    return "Desconocido"

def getNetmask(dev):
  logging.info("[Started] getNetmask call")                         
  try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(s.fileno(),0x891b,struct.pack('256s', dev))[20:24])
  except:
    logging.warning("[Finish] getMask call, UNKNOWN Mask")
    return "Desconocido"

def getGateway():
  logging.info("[Started] getGateway call")
  p = subprocess.Popen("/bin/ip route list | /usr/bin/awk ' /^default/ {print $3}'", stdout=subprocess.PIPE, shell=True)
  logging.debug("[Finished] getGateway call")
  return p.communicate()[0].rstrip()

def getNetInfo():
  logging.info("[Started] getNetInfo call")
  net = []
  net.append(getIP("wlan0"))
  net.append(getNetmask("wlan0"))
  net.append(getGateway())
  logging.debug("[Finish] diskUsage call, rerturned IP-> %s Mask-> %s GW-> %s" % (net[0], net[1], net[2]))
  return net

def getHostList(dev):
  #ToDo: Echarle una pensada
  #p = subprocess.Popen("nmap -sP $getIP("dev")/$MASK | grep report | sed -r 's/.* (.*\..*\..*\..*)/\\1/", stdout=subprocess.PIPE, shell=True)
  #return p.communicate()[0].rstrip()
  return None
