####################################################################################
# 
#   LCD.py
#
#    This file is part of smartPi proyect
#
#    smartPi is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    smartPi is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with smartPi.  If not, see <http://www.gnu.org/licenses/>.
#
####################################################################################
#
# The wiring for the LCD is as follows:
# 1 : GND
# 2 : 5V
# 3 : Contrast (0-5V)*
# 4 : RS (Register Select)
# 5 : R/W (Read Write)       - GROUND THIS PIN
# 6 : Enable or Strobe
# 7 : Data Bit 0             - NOT USED
# 8 : Data Bit 1             - NOT USED
# 9 : Data Bit 2             - NOT USED
# 10: Data Bit 3             - NOT USED
# 11: Data Bit 4
# 12: Data Bit 5
# 13: Data Bit 6
# 14: Data Bit 7
# 15: LCD Backlight +5V**
# 16: LCD Backlight GND

import RPi.GPIO as GPIO
import time
import logging

logging.basicConfig(format = '%(asctime)s - %(levelname)s - %(filename)s - %(lineno)d - %(message)s', level = logging.INFO)

# Define GPIO to LCD mapping
LCD_RS = 8
LCD_E  = 24
LCD_D4 = 23
LCD_D5 = 18
LCD_D6 = 15
LCD_D7 = 14

# Define some device constants
LCD_WIDTH = 16    # Maximum characters per line
LCD_CHR = True
LCD_CMD = False

LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line 
LCD_CHARS = [0x40,0x48,0x50,0x58,0x60,0x68,0x70,0x78]

# Timing constants
E_PULSE = 0.00005
E_DELAY = 0.00005

class LCD:
  blocked = False
  def __init__(self):
    logging.debug("Initializing LCD")
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)       # Use BCM GPIO numbers
    GPIO.setup(LCD_E, GPIO.OUT)  # E
    GPIO.setup(LCD_RS, GPIO.OUT) # RS
    GPIO.setup(LCD_D4, GPIO.OUT) # DB4
    GPIO.setup(LCD_D5, GPIO.OUT) # DB5
    GPIO.setup(LCD_D6, GPIO.OUT) # DB6
    GPIO.setup(LCD_D7, GPIO.OUT) # DB7

    # Initialise display
    self.lcd_byte(0x33, LCD_CMD)
    self.lcd_byte(0x32, LCD_CMD)
    self.lcd_byte(0x28, LCD_CMD)
    self.lcd_byte(0x0C, LCD_CMD)  
    self.lcd_byte(0x06, LCD_CMD)
    self.lcd_byte(0x01, LCD_CMD) 
	
  def showScroll(self, line, message, delay = 0.4):
    logging.info("Showing message in display with scroll")
    str_pad = " " *LCD_WIDTH
    my_long_string = str_pad + message
    for i in range (0, len(my_long_string)):
      lcd_text = my_long_string[i:(i+15)]
      self.show(line, lcd_text)
      time.sleep(delay)
    self.show(line, str_pad)
		
  def show(self, line, message):
    logging.debug("Showing message in display, blocked = %s" % (self.blocked))
    while self.blocked: continue
    self.blocked = True
    self.lcd_byte(line, LCD_CMD)
    message = message.ljust(LCD_WIDTH, " ")
    for i in range(LCD_WIDTH):
      self.lcd_byte(ord(message[i]), LCD_CHR)
    self.blocked = False
			
  def custom(self, charPos, charDef):
    logging.info("Creating a custom char")
    self.lcd_byte(LCD_CHARS[charPos], LCD_CMD)
    for line in charDef:
      self.lcd_byte(line, LCD_CHR)
		  
  def lcd_byte(self, bits, mode):
    # Send byte to data pins
    # bits = data
    # mode = True  for character
    #        False for command
    
    GPIO.output(LCD_RS, mode) # RS
		
    # High bits
    GPIO.output(LCD_D4, False)
    GPIO.output(LCD_D5, False)
    GPIO.output(LCD_D6, False)
    GPIO.output(LCD_D7, False)
    if bits & 0x10 == 0x10:
      GPIO.output(LCD_D4, True)
    if bits & 0x20 == 0x20:
      GPIO.output(LCD_D5, True)
    if bits & 0x40 == 0x40:
      GPIO.output(LCD_D6, True)
    if bits & 0x80 == 0x80:
      GPIO.output(LCD_D7, True)
		
    # Toggle 'Enable' pin
    time.sleep(E_DELAY)    
    GPIO.output(LCD_E, True)  
    time.sleep(E_PULSE)
    GPIO.output(LCD_E, False)  
    time.sleep(E_DELAY)      
		
    # Low bits
    GPIO.output(LCD_D4, False)
    GPIO.output(LCD_D5, False)
    GPIO.output(LCD_D6, False)
    GPIO.output(LCD_D7, False)
    if bits & 0x01 == 0x01:
      GPIO.output(LCD_D4, True)
    if bits & 0x02 == 0x02:
      GPIO.output(LCD_D5, True)
    if bits & 0x04 == 0x04:
      GPIO.output(LCD_D6, True)
    if bits & 0x08 == 0x08:
      GPIO.output(LCD_D7, True)
		
    # Toggle 'Enable' pin
    time.sleep(E_DELAY)    
    GPIO.output(LCD_E, True)  
    time.sleep(E_PULSE)
    GPIO.output(LCD_E, False)  
    time.sleep(E_DELAY)
		
  def clear(self):
    logging.info("Clearing display")
    # Initialise display
    self.lcd_byte(0x33,LCD_CMD)
    self.lcd_byte(0x32,LCD_CMD)
    self.lcd_byte(0x28,LCD_CMD)
    self.lcd_byte(0x0C,LCD_CMD)  
    self.lcd_byte(0x06,LCD_CMD)
    self.lcd_byte(0x01,LCD_CMD)
