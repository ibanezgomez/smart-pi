####################################################################################
# 
#   Syscall.py
#
#    This file is part of smartPi proyect
#
#    smartPi is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    smartPi is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with smartPi.  If not, see <http://www.gnu.org/licenses/>.
#
####################################################################################

import subprocess
import time
import datetime
import logging

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(filename)s - %(lineno)d - %(message)s' ,level=logging.INFO)


#   -> Get device status
# 1 - CPU usage & Temperature
# 2 - RAM free and Clock
def raspStatus():
  logging.info("[Started] raspStatus call")
  statusParse = []
  s = subprocess.Popen("ps aux | awk 'NR > 0 { s +=$3 }; END {print s}'", stdout = subprocess.PIPE, shell = True)
  out = "CPU: %s" % (s.communicate()[0].strip()) + "%"
  size = 16 - len(out)
  while size != 4:
    out = out + " "
    size = 16 - len(out)
  s = subprocess.Popen("/opt/vc/bin/vcgencmd measure_temp | sed -r 's/.*=([0-9]*).*/\\1/g'", stdout = subprocess.PIPE, shell = True)
  statusParse.append(out + s.communicate()[0].strip() + chr(223) + "C")

  s = subprocess.Popen("cat /proc/meminfo | grep MemFree | sed -r 's/.* (.*[0-9]) .*/\\1\/1024/g' | bc", stdout = subprocess.PIPE, shell = True)
  out = "RAM: %sMB" % (s.communicate()[0].strip())
  size = 16 - len(out)
  while size != 5:
   out = out + " "
   size = 16 - len(out)
  statusParse.append(out + datetime.datetime.now().strftime("%H:%M"))
  logging.debug("[Finish] raspStatus call, return value L1: %s ; L2: %s" % (statusParse[0], statusParse[1]))
  return statusParse

#   -> Get a list with download torrents status
# n - Format: XX%-XXXXXXXXXXX
def torrentStatus():
  logging.info("[Started] torrentStatus call")
  torrentParse = []
  t = subprocess.Popen("transmission-remote 12.9.89.102:9091 -n transmission:transmission -l | grep % | sed -r 's/.* (.*'%').*  (.*)/\\1-\\2                /g' |  cut -c 1-16", stdout = subprocess.PIPE, shell = True)
  torrentParse.append(t.communicate()[0].split("\n"))
  torrentParse[0].pop()
  torrentParse = torrentParse[0]
  logging.debug("[Finish] torrentStatus call, length array returned: %d" % (len(torrentParse)))
  return torrentParse

#   -> Check if a rsync instance is executing
# n - True/False
def isRSYNCActive():
  logging.info("[Started] isRSYNCActive call")
  r = subprocess.Popen("ps -e | grep rsync | head -1 | sed -r 's/.* (.*)/1/g'", stdout = subprocess.PIPE, shell = True)
  logging.debug("[Finish] isRSYNCActive call")
  if r.communicate()[0].strip() == "1":
    return True
  return False

#   -> Get a list about current ssh sessions
# n - user//host
def sshConnections():
  logging.info("[Started] sshConnections call")
  c = subprocess.Popen("who | grep pts | sed -r 's/(.*).*pts.*\((.*)\).*/\\1\\2/g' ", stdout = subprocess.PIPE, shell = True)
  logging.debug("[Finish] sshConnections call")
  return c.communicate()[0].split()

def diskUsage():
  logging.info("[Started] diskUsage call")
  d = subprocess.Popen("df -H | grep -vE '^Filesystem|tmpfs|cdrom|ficheros' | awk '{ print  $1 \" usado al \" $5 \", disp. \" $4 \"        \" }'| tr \"\n\" \" \"", stdout = subprocess.PIPE, shell = True)
  logging.debug("[Finish] diskUsage call")
  return d.communicate()[0][:-9]

def startLCDd():
  logging.info("[Started] startLCDd call, stating daemon") 
  subprocess.Popen("sudo /etc/init.d/LCDd start", stdout = subprocess.PIPE, shell = True)
  logging.debug("[Finish] startLCDd call")

def stopLCDd():
  logging.info("[Started] stopLCDd call, killing all instaces of LCDd & lcdproc")
  subprocess.Popen("sudo /etc/init.d/LCDd stop", stdout = subprocess.PIPE, shell = True)
  subprocess.Popen("sudo pkill lcdproc", stdout = subprocess.PIPE, shell = True)
  time.sleep(2)
  logging.debug("[Finish] stopLCDd call")

