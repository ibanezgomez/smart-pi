####################################################################################
# 
#   main.py
#
#    This file is part of smartPi proyect
#
#    smartPi is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    smartPi is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with smartPi.  If not, see <http://www.gnu.org/licenses/>.
#
####################################################################################

import time
import datetime
import logging

from LCD import *
from Network import *
from Syscall import *

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(filename)s - %(lineno)d - %(message)s' ,level = logging.INFO)

logging.info('*** Starting sMaRt Pi LCD checking status - Samuel 2012 ***')

try:
  stopLCDd()
 
  #Initialise display
  lcd = LCD()
  time.sleep(1)
  logging.info("Starting DEMO animation")
  lcd.show(LCD_LINE_1, " >-          -> ")
  time.sleep(0.5)
  lcd.show(LCD_LINE_1, " >- s        -> ")
  time.sleep(0.23)
  lcd.show(LCD_LINE_1, " >- s        -> ")
  time.sleep(0.23)
  lcd.show(LCD_LINE_1, " >- sM       -> ")
  time.sleep(0.23)
  lcd.show(LCD_LINE_1, " >- sMa      -> ")
  time.sleep(0.23)
  lcd.show(LCD_LINE_1, " >- sMaR     -> ")
  time.sleep(0.23)
  lcd.show(LCD_LINE_1, " >- sMaRt    -> ")
  time.sleep(0.23)
  lcd.show(LCD_LINE_1, " >- sMaRt    -> ")
  time.sleep(0.23)
  lcd.show(LCD_LINE_1, " >- sMaRt P  -> ")
  time.sleep(0.23)
  lcd.show(LCD_LINE_1, " >- sMaRt Pi -> ")
  time.sleep(0.23)
  lcd.showScroll(LCD_LINE_2, "Media Center powered by Raspberry %s - 54MU3L '12  " % chr(247), 0.25)
  time.sleep(1)

  while True:
    for i in range(3):
      buf = raspStatus()
      lcd.show(LCD_LINE_1, buf[0])
      lcd.show(LCD_LINE_2, buf[1])
      time.sleep(10)
    #change mode
    buf = getNetInfo()
    lcd.show(LCD_LINE_1, "Estado de la red")
    lcd.showScroll(LCD_LINE_2, "IP: %s      Masc: %s      GW: %s" % (buf[0], buf[1], buf[2]))
    #change mode
    for i in range(3):
      buf = raspStatus()
      lcd.show(LCD_LINE_1, buf[0])
      lcd.show(LCD_LINE_2, buf[1])
      time.sleep(10)
    #change mode
    buf = diskUsage()
    lcd.show(LCD_LINE_1, " Almacenamiento ")
    lcd.showScroll(LCD_LINE_2, buf, 0.25)
    #change mode
    if isRSYNCActive():
      lcd.show(LCD_LINE_1, "RSYNC en proceso")
      lcd.showScroll(LCD_LINE_2, ".:. .:. .:.", 0.2)
      while isRSYNCActive():
        lcd.showScroll(LCD_LINE_2, ".:. .:. .:.", 0.2)
    #change mode
    buf = sshConnections()
    if len(buf) != 0:
      lcd.show(LCD_LINE_1, " Conexiones SSH ")
      j = 0
      for i in range(len(buf)/2):
        lcd.showScroll(LCD_LINE_2, "< < Usuario %s desde %s < <" % (buf[j], buf[j+1]), 0.25)
        j = j+2
    #change mode
    buf = torrentStatus()
    if len(buf) != 0:
      lcd.show(LCD_LINE_1, "List. de Torrent")
      lcd.showScroll(LCD_LINE_2, ".T. .T. .T.", 0.2)
      if len(buf) == 1:
        lcd.show(LCD_LINE_1, buf[0])
        lcd.show(LCD_LINE_2, "")
        time.sleep(1)
      if len(buf)%2 == 0:
        buf.append("")
      for j in range(len(buf)-1):
        lcd.show(LCD_LINE_1, buf[j])
        lcd.show(LCD_LINE_2, buf[j+1])
        if j != len(buf)-2:
  	  time.sleep(3)

except KeyboardInterrupt:
  logging.error("Keyboard Interrupt")    
  GPIO.cleanup()
