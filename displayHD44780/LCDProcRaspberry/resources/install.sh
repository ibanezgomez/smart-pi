####################################################################################
# 
#   install.sh
#
#    This file is part of smartPi proyect
#
#    smartPi is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    smartPi is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with smartPi.  If not, see <http://www.gnu.org/licenses/>.
#
####################################################################################

# Install lcdproc
sudo apt-get install lcdproc

# Download the drivers
tar xzvf driverLCD.tar.gz

#Copy drivers to home directory
mv lcdproc /home/pi/

#Rewrite config file
sudo cp LCDd.conf /etc/

#Rewrite widgets with BClock and CPU
#sudo cp lcdproc.conf /etc/

# Start service
sudo /etc/init.d/LCDd restart

#Appendix
#
# Configuring Raspbmc
#
# Going into System menu on the far right, go into System and in Video output you’ll have a check box labelled “Enable LCD/VFD”, click that and instantly the display will start outputting the current status on the display.
#
# As most LCD screens have poor refresh rates and most information provided by XBMC is too long to fit, it will start to scroll the data, resulting in a blurry mess.
#
# What we need to do, is to cut this information down via the configuration files. These files are located in “/home/pi/.xbmc/userdata”. Simply type in the command box:
#
#     cd /home/pi/.xbmc/userdata
#
# Then:
#
#     sudo nano LCD.xml
#
# This will now display line by line so make any amendments needed. All the information you will need is detailed on the official XBMC wiki page.
#
# BONUS - Enable with terminal:
# 
#    nano /home/pi/.xbmc/userdata/LCD.xml and change <haslcd>false</haslcd> to true
#
